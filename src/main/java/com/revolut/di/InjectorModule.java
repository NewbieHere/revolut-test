package com.revolut.di;

import com.google.inject.AbstractModule;
import com.revolut.service.api.*;
import com.revolut.service.impl.*;

public class InjectorModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(DatabaseManager.class).to(DatabaseManagerImpl.class);
        bind(AccountLockManager.class).to(AccountLockManagerImpl.class);
        bind(ExchangeService.class).to(ExchangeServiceImpl.class);
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(MoneyService.class).to(MoneyServiceImpl.class);

    }
}
