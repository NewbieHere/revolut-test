package com.revolut.di;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class InjectorProvider {
    private static Injector injector;

    static {
        injector = Guice.createInjector(new InjectorModule());
    }

    public static Injector provide() {
        return injector;
    }
}
