package com.revolut.util;

import org.flywaydb.core.Flyway;

import java.util.Properties;

public class MigrationLoader {
    public static void main(String[] args) {
        run();
    }

    public static void run() {
        Properties props = Utils.loadJdbcProperties();
        Flyway flyway = new Flyway();
        flyway.setDataSource(
                props.getProperty("jdbc.url"),
                props.getProperty("jdbc.username"),
                props.getProperty("jdbc.password"));
        flyway.migrate();
    }
}
